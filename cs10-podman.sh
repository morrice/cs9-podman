#!/bin/bash
BASEURL=https://composes.stream.centos.org/stream-10/production/latest-CentOS-Stream/compose/
FILENAME=`curl --silent ${BASEURL}/BaseOS/x86_64/images/ |grep "tar.xz</a>" | cut -d\" -f6`
CS10TEMP=`mktemp -d`
echo "Downloading ${FILENAME} to ${CS10TEMP}"
curl --silent ${BASEURL}/BaseOS/x86_64/images/${FILENAME} --output - > ${CS10TEMP}/cs10.xz
echo "extracting stuff"
tar xfJ ${CS10TEMP}/cs10.xz -C ${CS10TEMP}
LAYER=`find ${CS10TEMP} -name layer.tar`
mkdir ${CS10TEMP}/rootfs
tar xf ${LAYER} -C ${CS10TEMP}/rootfs
echo "creating podman image"
chmod a+w ${CS10TEMP}/rootfs/
cat << EOF > ${CS10TEMP}/rootfs/Dockerfile
FROM scratch
ADD . /
ENTRYPOINT /bin/bash
EOF
podman build -t cs10 ${CS10TEMP}/rootfs/
echo "all done!"
echo "You can now enjoy brokenedge stuff with:"
echo "podman run -it cs10"
